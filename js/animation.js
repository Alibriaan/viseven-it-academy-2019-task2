function activeStage() {
    // Получение необходимых блоков
    var submarine = document.querySelector('.submarine-wrapper');
    var rocket = document.querySelector('.rocket-wrapper');
    var iceberg = document.querySelector('.iceberg-destroy');

    // Активация активной фазы субмарины и айсберга
    submarine.addEventListener("animationiteration", function changeStageSubmarine(e) {

        if (e.animationName === 'submarine_move') {
            submarine.classList.add('active');
            iceberg.classList.add('active');
        }

    }, false);

    // Активация активной фазы ракет
    rocket.addEventListener("animationiteration", function changeStageRocket(e) {
        if (e.animationName === 'rocket_move' && submarine.classList.length === 2) {
            rocket.classList.add('active');
        }

    }, false);
}